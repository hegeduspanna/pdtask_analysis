rootdir = 'E:\drive_backup\Experiments\Human_SSRT\Analysis\Human';
humanIDs = {'n001' 'n003' 'n004' 'n005' 'n007' 'n008'};
medianRT = nan(length(humanIDs), 6);
pooledmedianRT = nan(length(humanIDs), 3);

preopFART = [];
stimonFART = [];
stimoffFART = [];


for i=1:length(humanIDs)
    humanID = humanIDs{i};
    resdir = [rootdir '\' humanID '\Figs'];
    sessionpath = (fullfile(rootdir, humanID));
    filelist = dir(fullfile(sessionpath, '*.mat'));
    filenum = size(filelist,1);

    for j=1:filenum
        currentFile = filelist(j).name;
        load([rootdir '\' humanID '\' currentFile]); %load Matlab file
        Latency_ARD = Results.Trial.RT1_ARD-Results.Trial.CUE_ARD;   % RT in ARD first press
       StopTrials = Results.Trial.outcomeCode==5;
        FART = Latency_ARD(StopTrials);
        mFART = nanmedian(FART);
        
        if  ~isempty(strfind(currentFile, 'preop'))
            if ~isempty(strfind(currentFile, 'left'))
                medianRT(i, 1) = mFART;
            else
                medianRT(i, 4) = mFART;
            end
            preopFART = [preopFART FART];
        elseif  ~isempty(strfind(currentFile, 'stimon'))
            if ~isempty(strfind(currentFile, 'left'))
                medianRT(i, 2) = mFART;
            else
                medianRT(i, 5) = mFART;
            end
            stimonFART = [stimonFART FART];
        elseif  ~isempty(strfind(currentFile, 'stimoff'))
            if ~isempty(strfind(currentFile, 'left'))
                medianRT(i, 3) = mFART;
            else
                medianRT(i, 6) = mFART;
            end
            stimoffFART = [stimoffFART FART];
        end
    end
    
    pooledmedianRT(i,:) = [median(preopFART) median(stimonFART) median(stimoffFART)];
    
    
end


         %compare sides - see if we can merge the data
    [H1, Wp1] = boxstat(medianRT(:,1),medianRT(:,4),'left side','right side', 0.005);
    title('Preop. GoRT')
    saveas(H1,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ 'preop_FART_test.fig']));

    [H2, Wp2] = boxstat(medianRT(:,2),medianRT(:,5),'left side','right side', 0.005);
    title('Postop. stimon GoRT')
    saveas(H2,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ 'postop_stimon_FART_test.fig']));

    [H3, Wp3] = boxstat(medianRT(:,3),medianRT(:,6),'left side','right side', 0.005);
    title('Postop. stimoff GoRT')
    saveas(H3,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ 'postop_stimoff_FART_test.fig']));
    
    keyboard