function PD_behavior_main(humanID)

%Main function for behavior analysis of preop intraop and postop groups


rootdir = 'E:\drive_backup\Experiments\Human_SSRT\Analysis\Human';
resdir = [rootdir '\' humanID '\Figs'];
sessionpath = (fullfile(rootdir, humanID));
filelist = dir(fullfile(sessionpath, '*.mat'));
filenum = size(filelist,1);

Patient = struct;
Patient.Preop = struct;
Patient.Intraop = struct;
Patient.Postop_stimon = struct;
Patient.Postop_stimoff = struct;

Performance = nan(2,4);
StopIndex = nan(2,4);
Stop_prop = nan(1,12);

for i=1:filenum
    currentFile = filelist(i).name;
    
    if ~isempty(strfind(currentFile, 'left'))
        side = 'left'; %left
        tag = 1;
    else
        side = 'right'; %right
        tag = 2;
    end
    
    load([rootdir '\' humanID '\' currentFile]); %load Matlab file
    
    NumFlip = length(Results.Trial.flipTime);   % Number of Cue presentations
    Latency_ARD = Results.Trial.RT1_ARD-Results.Trial.CUE_ARD;   % RT in ARD first press
    Latency_ARD2 = Results.Trial.RT2_ARD-Results.Trial.CUE_ARD; %RT in ARD second press
    
    % KEY COMBINATIONS
    keyComb(:,1) = Results.Trial.Key1';
    keyComb(:,2) = Results.Trial.Key2';
    [newNorm, newRev] = deal(zeros(NumFlip, 1));
    for iT = 1:NumFlip %Checking for new key combinations
        if keyComb(iT,1) == 2 && keyComb(iT,2)==3
            newNorm(iT) = 1;
        end
        if keyComb(iT,1) == 3 && keyComb(iT,2)==1
            newRev(iT) = 1;
        end
    end
    
    %Performance
    Hit = Results.Trial.outcomeCode(1:NumFlip) == 1;
    NoStopTrials = Results.Trial.outcomeCode==1 | Results.Trial.outcomeCode ==2;
    perfind = sum(Hit)/sum(NoStopTrials)
    
    %Successful stops
    SuccesfulStop = Results.Trial.Outcome(1:NumFlip) == 4;
    AllStop = Results.Trial.Outcome==3 | Results.Trial.Outcome ==4;
    stopind = sum(SuccesfulStop)/sum(AllStop);
    
    if ~isempty(strfind(currentFile, 'preop'))
        [Patient.Preop] = getlatencies(Patient.Preop, newNorm, newRev ,side, Latency_ARD);
        Performance(tag, 1) = perfind;
        StopIndex(tag,1) = stopind;
        if tag ==1
            Stop_prop(1:2) = [sum(SuccesfulStop) sum(AllStop)];
        elseif tag ==2
            Stop_prop(7:8) = [sum(SuccesfulStop) sum(AllStop)];
        end
    elseif ~isempty(strfind(currentFile, 'intraop'))
        [Patient.Intraop] = getlatencies(Patient.Intraop, newNorm, newRev ,side, Latency_ARD);
        Performance(tag, 2) = perfind;
        StopIndex(tag,2) = stopind;
        
        %New Key combinations intraop
        newInx = logical(newNorm+newRev);
        normLat = Latency_ARD(logical(newNorm));
        revLat = Latency_ARD(logical(newRev));
        learnedLat = Latency_ARD(~newInx);
        newLat = Latency_ARD(newInx);
        
        %Figure
        [H3, Wp1] = boxstat(normLat,revLat,'new_normal','new_reversed', 0.005);
        title('Intraop RT')
        delete(findobj(gcf,'tag','Outliers'))
        set(gca, 'YLimMode', 'auto')
        
        saveas(H3,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ humanID '_RT_keys_rev_vs_learned_intraop_' side '.fig']));
        saveas(H3,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ humanID '_RT_keys_rev_vs_learned_intraop_' side '.jpg']));
        set(H3,'Renderer','painters');
        saveas(H3,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ humanID '_RT_keys_rev_vs_learned_intraop_' side '.eps']));
        close all
        
        
    elseif ~isempty(strfind(currentFile, 'stimon'))
        [Patient.Postop_stimon] = getlatencies(Patient.Postop_stimon, newNorm, newRev ,side, Latency_ARD);
        Performance(tag, 3) = perfind;
        StopIndex(tag,3) = stopind;
        if tag ==1
            Stop_prop(3:4) = [sum(SuccesfulStop) sum(AllStop)];
        elseif tag ==2
            Stop_prop(9:10) = [sum(SuccesfulStop) sum(AllStop)];
        end
    elseif ~isempty(strfind(currentFile, 'stimoff'))
        [Patient.Postop_stimoff] = getlatencies(Patient.Postop_stimoff, newNorm, newRev ,side, Latency_ARD);
        StopIndex(tag, 4) = stopind;
        if tag ==1
            Stop_prop(5:6) = [sum(SuccesfulStop) sum(AllStop)];
        elseif tag ==2
            Stop_prop(11:12) = [sum(SuccesfulStop) sum(AllStop)];
        end
    end
    
end
succesful_stops(StopIndex, resdir);
compare_performance(Performance, resdir);
close all
compare_RTs_individual(humanID, Patient, 'E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior\');
% keyboard

function [Lats] = getlatencies(Lats, newNorm, newRev ,sides, Latency_ARD)
if strcmp(sides, 'left')
    newInx = logical(newNorm+newRev); %new trials incorporated (intra and postop)
    Lats.left.allLat = Latency_ARD;
    Lats.left.medianallLat = nanmedian(Latency_ARD);
    Lats.left.normLat = Latency_ARD(logical(newNorm)); %new trials - numbers in descending order
    Lats.left.revLat = Latency_ARD(logical(newRev)); %new trials - numbersi n ascending order
    Lats.left.learnedLat = Latency_ARD(~newInx); %old trials - introduced during preop measurement
    Lats.left.newLat = Latency_ARD(newInx); %old trials - introduced during preop measurement
elseif strcmp(sides, 'right')
    newInx = logical(newNorm+newRev); %new trials incorporated (intra and postop)
    Lats.right.allLat = Latency_ARD;
    Lats.right.medianallLat = nanmedian(Latency_ARD);
    Lats.right.normLat = Latency_ARD(logical(newNorm)); %new trials - numbers in descending order
    Lats.right.revLat = Latency_ARD(logical(newRev)); %new trials - numbersi n ascending order
    Lats.right.learnedLat = Latency_ARD(~newInx); %old trials - introduced during preop measurement
    Lats.right.newLat = Latency_ARD(newInx); %old trials - introduced during preop measurement
end