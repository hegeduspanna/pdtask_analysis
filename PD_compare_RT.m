function PD_compare_RT(humanID, sidespec)

%Comparing reaction times of pre- intra and postop SSRT measurements in
%patient defined by HUMANID.

rootdir = 'F:\Human';


    
    Preop_Latency_ARD_all = []
    Intraop_Latency_ARD_all = []
    Postop_stimon_Latency_ARD_all = []
    Postop_stimoff_Latency_ARD_all = []

for k = 1:length(humanID);
    chumanID = humanID{k};
    resdir = [rootdir '\' chumanID '\Figs'];
    sessionpath = (fullfile(rootdir, chumanID));
    filelist = dir(fullfile(sessionpath, '*.mat'));
    filenum = size(filelist,1);

    
    for i=1:filenum
        currentFile = filelist(i).name;
        if ~isempty(strfind(currentFile, sidespec)) %if the good side
            load([rootdir '\' chumanID '\' currentFile]);
            if ~isempty(strfind(currentFile, 'preop'))
                Preop_Latency_ARD = Results.Trial.RT1_ARD-Results.Trial.CUE_ARD;   % RT in ARD
                Preop_Latency_ARD_all = [Preop_Latency_ARD_all, Preop_Latency_ARD]
            elseif ~isempty(strfind(currentFile, 'intraop'))
                Intraop_Latency_ARD = Results.Trial.RT1_ARD-Results.Trial.CUE_ARD;   % RT in ARD
                Intraop_Latency_ARD_all = [Intraop_Latency_ARD_all, Intraop_Latency_ARD];
            elseif ~isempty(strfind(currentFile, 'stimon'))
                Postop_stimon_Latency_ARD =  Results.Trial.RT1_ARD-Results.Trial.CUE_ARD;   % RT in ARD
                Postop_stimon_Latency_ARD_all = [Postop_stimon_Latency_ARD_all, Postop_stimon_Latency_ARD];
            elseif ~isempty(strfind(currentFile, 'stimoff'))
                Postop_stimoff_Latency_ARD = Results.Trial.RT1_ARD-Results.Trial.CUE_ARD;   % RT in ARD
                Postop_stimoff_Latency_ARD_all = [Postop_stimoff_Latency_ARD_all, Postop_stimoff_Latency_ARD];
            end
        end
    end
    
    %Comparing overall RT
H = figure;
hold on
YScale = get(gca, 'YLim');

if exist('Preop_Latency_ARD', 'var')
bar(1, nanmedian(Preop_Latency_ARD), 'FaceColor', 'blue')
plot(1, Preop_Latency_ARD,'*','MarkerSize',6,'MarkerFaceColor',[0 1 0],'MarkerEdgeColor','k')  % scatter plot
text(0.9, YScale(2)-50, 'Preop')
end

if exist('Intraop_Latency_ARD', 'var')
bar(2, nanmedian(Intraop_Latency_ARD), 'FaceColor', 'blue')
plot(2, Intraop_Latency_ARD,'*','MarkerSize',6,'MarkerFaceColor',[1 0 0],'MarkerEdgeColor','k')  % scatter plot
text(1.9, YScale(2)-50, 'Intraop')
end

if ~isvarname(Postop_stimon_Latency_ARD)
bar(3, nanmedian(Postop_stimon_Latency_ARD), 'FaceColor', 'blue')
plot(3, Postop_stimon_Latency_ARD,'*','MarkerSize',6,'MarkerFaceColor',[0 0 1],'MarkerEdgeColor','k')  % scatter plot
text(2.75, YScale(2)-50, 'Postop stimon')
end

if ~isempty(Postop_stimoff_Latency_ARD)
bar(4, nanmedian(Postop_stimoff_Latency_ARD), 'FaceColor', 'blue')
plot(4, Postop_stimoff_Latency_ARD,'*','MarkerSize',6,'MarkerFaceColor',[0 1 1],'MarkerEdgeColor','k')  % scatter plot
text(3.75, YScale(2)-50, 'Postop stimoff')
end


ylabel('Milliseconds')
set(gca,'xticklabel',[]);
title(['Overall RT ' sidespec]);
savefig(H, [resdir '\' chumanID '_' sidespec '_RT.fig']);
saveas(H, [resdir '\' chumanID '_' sidespec '_RT.jpg']);
close(H)
    
end

% keyboard


H = figure;
hold on
YScale = get(gca, 'YLim');


bar(1, median(nanmedian(Preop_Latency_ARD_all)), 'FaceColor', 'blue')
plot(1, Preop_Latency_ARD_all,'*','MarkerSize',6,'MarkerFaceColor',[0 1 0],'MarkerEdgeColor','k')  % scatter plot
text(0.9, YScale(2)-50, 'Preop')

bar(2, median(nanmedian(Intraop_Latency_ARD_all)), 'FaceColor', 'blue')
plot(2, Intraop_Latency_ARD_all,'*','MarkerSize',6,'MarkerFaceColor',[1 0 0],'MarkerEdgeColor','k')  % scatter plot
text(1.9, YScale(2)-50, 'Intraop')


bar(3, median(nanmedian(Postop_stimon_Latency_ARD_all)), 'FaceColor', 'blue')
plot(3, Postop_stimon_Latency_ARD_all,'*','MarkerSize',6,'MarkerFaceColor',[0 0 1],'MarkerEdgeColor','k')  % scatter plot
text(2.75, YScale(2)-50, 'Postop stimon')


bar(4, median(nanmedian(Postop_stimoff_Latency_ARD_all)), 'FaceColor', 'blue')
plot(4, Postop_stimoff_Latency_ARD_all,'*','MarkerSize',6,'MarkerFaceColor',[0 1 1],'MarkerEdgeColor','k')  % scatter plot
text(3.75, YScale(2)-50, 'Postop stimoff')



ylabel('Milliseconds')
set(gca,'xticklabel',[]);

title(['Overall RT ' sidespec]);
savefig(H, ['F:\Human\_behavior\'  sidespec 'overall_RT.fig']);
saveas(H, ['F:\Human\_behavior\' sidespec 'overall_RT.jpg']);
close(H)