


rootdir = 'E:\drive_backup\Experiments\Human_SSRT\Analysis\Human';
humanIDs = {'n001' 'n003' 'n004' 'n005' 'n007' 'n008'};
medianRT = nan(length(humanIDs), 6);
pooledmedianRT = nan(length(humanIDs), 3);

preopGoRT = [];
stimonGoRT = [];
stimoffGoRT = [];


for i=1:length(humanIDs)
    humanID = humanIDs{i};
    resdir = [rootdir '\' humanID '\Figs'];
    sessionpath = (fullfile(rootdir, humanID));
    filelist = dir(fullfile(sessionpath, '*.mat'));
    filenum = size(filelist,1);

    for j=1:filenum
        currentFile = filelist(j).name;
        load([rootdir '\' humanID '\' currentFile]); %load Matlab file
        Latency_ARD = Results.Trial.RT1_ARD-Results.Trial.CUE_ARD;   % RT in ARD first press
        NoStopTrials = Results.Trial.outcomeCode==1 | Results.Trial.outcomeCode ==2 |  Results.Trial.outcomeCode ==3 |  Results.Trial.outcomeCode ==4 ;
        GoRT = Latency_ARD(NoStopTrials);
        mGoRT = nanmedian(GoRT);
        
        if  ~isempty(strfind(currentFile, 'preop'))
            if ~isempty(strfind(currentFile, 'left'))
                medianRT(i, 1) = mGoRT;
            else
                medianRT(i, 4) = mGoRT;
            end
            preopGoRT = [preopGoRT GoRT];
        elseif  ~isempty(strfind(currentFile, 'stimon'))
            if ~isempty(strfind(currentFile, 'left'))
                medianRT(i, 2) = mGoRT;
            else
                medianRT(i, 5) = mGoRT;
            end
            stimonGoRT = [stimonGoRT GoRT];
        elseif  ~isempty(strfind(currentFile, 'stimoff'))
            if ~isempty(strfind(currentFile, 'left'))
                medianRT(i, 3) = mGoRT;
            else
                medianRT(i, 6) = mGoRT;
            end
            stimoffGoRT = [stimoffGoRT GoRT];
        end
    end
    
    pooledmedianRT(i,:) = [median(preopGoRT) median(stimonGoRT) median(stimoffGoRT)];
    
    
end


         %compare sides - see if we can merge the data
    [H1, Wp1] = boxstat(medianRT(:,1),medianRT(:,4),'left side','right side', 0.005);
    title('Preop. GoRT')
    saveas(H1,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ 'preop_GoRT_test.fig']));

    [H2, Wp2] = boxstat(medianRT(:,2),medianRT(:,5),'left side','right side', 0.005);
    title('Postop. stimon GoRT')
    saveas(H2,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ 'postop_stimon_GoRT_test.fig']));

    [H3, Wp3] = boxstat(medianRT(:,3),medianRT(:,6),'left side','right side', 0.005);
    title('Postop. stimoff GoRT')
    saveas(H3,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ 'postop_stimoff_GoRT_test.fig']));
    
    keyboard