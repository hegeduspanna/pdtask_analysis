function compare_performance(perfdata, resdir)

tag = {'left', 'right'};
for i=1:2
    H3 = figure;
    hold on
    bar([1,2,3,4],perfdata(i, 1:4), 'FaceColor', 'blue')
    YScale = get(gca, 'YLim');
    text(0.9, YScale(2), 'Preop')
    text(1.9, YScale(2), 'Intraop')
    text(2.75, YScale(2), 'Postop_stimon')
    text(3.75, YScale(2), 'Postop_stimoff')
    ylabel('Hit/nTrials')
    set(gca,'xticklabel',[]);
    title(['Performance ' tag{i}]);
    close(H3)
end