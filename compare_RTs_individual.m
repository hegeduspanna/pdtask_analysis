function compare_RTs_individual(humanID, Patient, resdir)
% COMPARE_RTS_INDIVIDUAL(PATIENT, RESDIR) compares reaction time (first
% button press after cue) of pre- intra- and postoperative measurements of a patient (PATIENT) in
% an SSRT task. M-W test is used to determine the difference between the RTs.
% The result bar graph and statistics are then saved to RESDIR.

% LEFT SIDE
% F1 = figure;
% hold on;
% bar([1,2,3,4], [nanmedian(Patient.Preop.left.allLat), nanmedian(Patient.Intraop.left.allLat), nanmedian(Patient.Postop_stimon.left.allLat), nanmedian(Patient.Postop_stimoff.left.allLat)],'FaceColor', 'none', 'LineWidth',2)
% plot(1, Patient.Preop.left.allLat, '*', 'Color', [0 0 0.7], 'LineWidth',3)
% plot(2, Patient.Intraop.left.allLat, '*', 'Color', [0 0 0.7], 'LineWidth',3)
% plot(3, Patient.Postop_stimon.left.allLat, '*', 'Color', [0 0 0.7], 'LineWidth',3)
% plot(4, Patient.Postop_stimoff.left.allLat, '*', 'Color', [0 0 0.7], 'LineWidth',3)
%
% YScale = ylim;
% text(0.8, YScale(2)-50, 'Preop')
% text(1.8, YScale(2)-50, 'Intraop')
% text(2.8, YScale(2)-50, 'Postop_stimon')
% text(3.8, YScale(2)-50, 'Postop_stimoff')
%
% ylabel('RT (ms)');
% xlabel('Groups')
% title('Left side');
% axis square;
% % savefig(F1, [resdir '\' tag '_RT.fig']);
% % saveas(F1, [resdir '\' tag '_RT.jpg']);
%  close(F1)

%STATISTICS LEFT SIDE
reaction_times_left = [{Patient.Preop.left.allLat},{Patient.Intraop.left.allLat},{Patient.Postop_stimon.left.allLat},{Patient.Postop_stimoff.left.allLat}];
[H1, Wp1] = boxstat(reaction_times_left{3},reaction_times_left{4},'stimon','stimoff', 0.005);
delete(findobj(gcf,'tag','Outliers'))
set(gca, 'YLimMode', 'auto')
[H2, Wp2] = boxstat(reaction_times_left{1},reaction_times_left{3},'preop','stimon', 0.005);
delete(findobj(gcf,'tag','Outliers'))
set(gca, 'YLimMode', 'auto')
% Save figures
saveas(H1,fullfile(resdir,[ humanID '_RT_stim_left' '.fig']));
saveas(H1,fullfile(resdir,[ humanID '_RT_stim_left' '.jpg']));
set(H1,'Renderer','painters');
saveas(H1,fullfile(resdir,[ humanID '_RT_stim_left' '.eps']));

saveas(H2,fullfile(resdir, [ humanID '_RT_preop_left' '.fig']));
saveas(H2,fullfile(resdir,[ humanID '_RT_preop_left' '.jpg']));
set(H2,'Renderer','painters');
saveas(H2,fullfile(resdir,[ humanID '_RT_preop_left' '.eps']));


% F2 = figure;
% hold on;
% bar([1,2,3,4], [nanmedian(Patient.Preop.right.allLat),nanmedian(Patient.Intraop.right.allLat), nanmedian(Patient.Postop_stimon.right.allLat), nanmedian(Patient.Postop_stimoff.right.allLat)],'FaceColor', 'none', 'LineWidth',2)
% plot(1, Patient.Preop.right.allLat, '*', 'Color', [0 0 0.7], 'LineWidth',3)
% plot(2, Patient.Intraop.right.allLat, '*', 'Color', [0 0 0.7], 'LineWidth',3)
% plot(3, Patient.Postop_stimon.right.allLat, '*', 'Color', [0 0 0.7], 'LineWidth',3)
% plot(4, Patient.Postop_stimoff.right.allLat, '*', 'Color', [0 0 0.7], 'LineWidth',3)
%
% YScale = ylim;
% text(0.8, YScale(2)-50, 'Preop')
% text(1.8, YScale(2)-50, 'Intraop')
% text(2.8, YScale(2)-50, 'Postop_stimon')
% text(3.8, YScale(2)-50, 'Postop_stimoff')
%
% ylabel('RT (ms)');
% xlabel('Groups')
% title('Right side');
% axis square;
% % savefig(F2, [resdir '\' tag '_RT.fig']);
% % saveas(F2, [resdir '\' tag '_RT.jpg']);
%  close(F2)

%STATISTICS RIGHT SIDE
reaction_times_right = [{Patient.Preop.right.allLat}, {Patient.Postop_stimon.right.allLat},{Patient.Postop_stimoff.right.allLat}];
[H3, Wp1] = boxstat(reaction_times_right{2},reaction_times_right{3},'stimon','stimoff', 0.005);
delete(findobj(gcf,'tag','Outliers'))
set(gca, 'YLimMode', 'auto')
[H4, Wp2] = boxstat(reaction_times_right{1},reaction_times_right{2},'preop','stimon', 0.005);
delete(findobj(gcf,'tag','Outliers'))
set(gca, 'YLimMode', 'auto')

% Save figures
saveas(H3,fullfile(resdir,[ humanID '_RT_stim_right' '.fig']));
saveas(H3,fullfile(resdir,[ humanID '_RT_stim_right' '.jpg']));
set(H3,'Renderer','painters');
saveas(H3,fullfile(resdir,[ humanID '_RT_stim_right' '.eps']));

saveas(H4,fullfile(resdir, [ humanID '_RT_preop_right' '.fig']));
saveas(H4,fullfile(resdir,[ humanID '_RT_preopc_right' '.jpg']));
set(H4,'Renderer','painters');
saveas(H4,fullfile(resdir,[ humanID '_RT_preop_right' '.eps']));

% keyboard