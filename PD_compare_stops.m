function PD_compare_stops(humanID, sidespec)

%comparing the number of successful stops


rootdir = 'F:\Human';
preop_avg = [];
intraop_avg = [];
stimon_avg = [];
stimoff_avg = [];

for k = 1:length(humanID);
    chumanID = humanID{k};
    resdir = [rootdir '\' chumanID '\Figs'];

    sessionpath = (fullfile(rootdir, chumanID));
    filelist = dir(fullfile(sessionpath, '*.mat'));
    filenum = size(filelist,1);
    
    for i=1:filenum
        currentFile = filelist(i).name;
        if ~isempty(strfind(currentFile, sidespec)) %if the good side
            load([rootdir '\' chumanID '\' currentFile]);
            if ~isempty(strfind(currentFile, 'preop'))
                Preop_StopT = Results.Trial.STOP_ARD-Results.Trial.CUE_ARD; %stop signal reaction time
                Preop_Inhibited = Results.Trial.outcomeCode == 6;
                Preop_Keypress = Results.Trial.outcomeCode == 5;
                preop = find(Preop_StopT(Preop_Inhibited));
                preop_all = [preop, find(Preop_StopT(Preop_Keypress))]
                numpreop = length(preop)/length(preop_all);
                preop_avg = [preop_avg, numpreop];
            elseif ~isempty(strfind(currentFile, 'intraop'))
               Intraop_StopT = Results.Trial.STOP_ARD-Results.Trial.CUE_ARD; %stop signal reaction time
               Intraop_Inhibited = Results.Trial.outcomeCode == 6;
               Intraop_Keypress = Results.Trial.outcomeCode == 5;
               intraop = find(Intraop_StopT(Intraop_Inhibited));
               numintraop = length(intraop)/length(intraop_all);
               intraop_all = [intraop, find(Intraop_StopT(Intraop_Keypress))]
               intraop_avg = [intraop_avg, numintraop];
                   
            elseif ~isempty(strfind(currentFile, 'stimon'))
                Stimon_StopT = Results.Trial.STOP_ARD-Results.Trial.CUE_ARD; %stop signal reaction time
                Stimon_Inhibited = Results.Trial.outcomeCode == 6;
                Stimon_Keypress = Results.Trial.outcomeCode == 5;
                stimon = find(Stimon_StopT(Stimon_Inhibited));
                stimon_all = [stimon, find(Stimon_StopT(Stimon_Keypress))];
            elseif ~isempty(strfind(currentFile, 'stimoff'))
                Stimoff_StopT = Results.Trial.STOP_ARD-Results.Trial.CUE_ARD; %stop signal reaction time
                Stimoff_Inhibited = Results.Trial.outcomeCode == 6;
                Stimoff_Keypress = Results.Trial.outcomeCode == 5;
                stimoff = find(Stimoff_StopT(Stimoff_Inhibited));
                stimoff_all = [stimoff, find(Stimoff_StopT(Stimoff_Keypress))];
            end
        end
    end
    
    
H = figure;
hold on
YScale = get(gca, 'YLim');

if exist('preop', 'var')
bar(1, numpreop, 'FaceColor', 'blue')
text(0.9, YScale(1)-0.05, 'Preop')
end

if exist('intraop', 'var')
bar(2, numintraop, 'FaceColor', 'blue')
text(1.9, YScale(1)-0.05, 'Intraop')
end

bar(3, length(stimon)/length(stimon_all), 'FaceColor', 'blue')
text(2.75, YScale(1)-0.05, 'Postop stimon')


bar(4, length(stimoff)/length(stimoff_all), 'FaceColor', 'blue')
text(3.75, YScale(1)-0.05, 'Postop stimoff')

ylabel('Milliseconds')
set(gca,'xticklabel',[]);
title(['Number of successful stops ' sidespec]);
savefig(H, [resdir '\' chumanID '_' sidespec '_InhSTOP_num.fig']);
saveas(H, [resdir '\' chumanID '_' sidespec '_InhSTOP_num.jpg']);
close(H)
end

