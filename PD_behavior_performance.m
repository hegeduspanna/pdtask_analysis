
rootdir = 'E:\drive_backup\Experiments\Human_SSRT\Analysis\Human';
humanIDs = {'n001' 'n003' 'n004' 'n005' 'n007' 'n008'};
Performance = nan(length(humanIDs), 6);
Perfomance_detailed = nan(length(humanIDs), 12);

for i=1:length(humanIDs)
    humanID = humanIDs{i};
    resdir = [rootdir '\' humanID '\Figs'];
    sessionpath = (fullfile(rootdir, humanID));
    filelist = dir(fullfile(sessionpath, '*.mat'));
    filenum = size(filelist,1);
    for j=1:filenum
        currentFile = filelist(j).name;
        load([rootdir '\' humanID '\' currentFile]); %load Matlab file
        
        %Performance
        NumFlip = length(Results.Trial.flipTime);   % Number of Cue presentations
        Hit = Results.Trial.outcomeCode(1:NumFlip) == 1;
        NoStopTrials = Results.Trial.outcomeCode==1 | Results.Trial.outcomeCode ==2;
        perfind = sum(Hit)/sum(NoStopTrials)
        
        if ~isempty(strfind(currentFile, 'preop'))
            if ~isempty(strfind(currentFile, 'left'))
                Performance(i, 1) = perfind;
                Perfomance_detailed(i,1) = sum(Hit);
                Perfomance_detailed(i,2) = NumFlip;
            else
                Performance(i, 4) = perfind;
                Perfomance_detailed(i,7) = sum(Hit);
                Perfomance_detailed(i,8) = NumFlip;
            end
        elseif ~isempty(strfind(currentFile, 'stimon'))
            if ~isempty(strfind(currentFile, 'left'))
                Performance(i, 2) = perfind;
                Perfomance_detailed(i,3) = sum(Hit);
                Perfomance_detailed(i,4) = NumFlip;
            else
                Performance(i, 5) = perfind;
                Perfomance_detailed(i,9) = sum(Hit);
                Perfomance_detailed(i,10) = NumFlip;
            end
        elseif ~isempty(strfind(currentFile, 'stimoff'))
            if ~isempty(strfind(currentFile, 'left'))
                Performance(i, 3) = perfind;
                Perfomance_detailed(i,5) = sum(Hit);
                Perfomance_detailed(i,6) = NumFlip;
            else
                Performance(i, 6) = perfind;
                Perfomance_detailed(i,11) = sum(Hit);
                Perfomance_detailed(i,12) = NumFlip;
            end
        end
    end   
    
end

 %compare sides - see if we can merge the data
    [H1, Wp1] = boxstat(Performance(:,1),Performance(:,4),'left side','right side', 0.005);
    title('Preop. performance')
    saveas(H1,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ 'preop_performance_test.fig']));

    [H2, Wp2] = boxstat(Performance(:,2),Performance(:,5),'left side','right side', 0.005);
    title('Postop. stimon performance')
    saveas(H2,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ 'postop_stimon_performance_test.fig']));

    [H3, Wp3] = boxstat(Performance(:,3),Performance(:,6),'left side','right side', 0.005);
    title('Postop. stimoff performance')
    saveas(H3,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[ 'postop_stimoff_performance_test.fig']));

keyboard