rootdir = 'E:\drive_backup\Experiments\Human_SSRT\Analysis\Human';
humanIDs = {'n001' 'n003' 'n004' 'n005' 'n007' 'n008'};

for i=1:length(humanIDs)
    humanID = humanIDs{i};
    resdir = [rootdir '\' humanID '\Figs'];
    sessionpath = (fullfile(rootdir, humanID));
    filelist = dir(fullfile(sessionpath, '*.mat'));
    filenum = size(filelist,1);
    figure;
    hold on
    for j=1:filenum
        currentFile = filelist(j).name;
        load([rootdir '\' humanID '\' currentFile]); %load Matlab file
        
        StopT = Results.Trial.STOP_ARD-Results.Trial.CUE_ARD;
        StopT = StopT(StopT>0);
        if  ~isempty(strfind(currentFile, 'preop'))
            if ~isempty(strfind(currentFile, 'left'))
                plot(StopT, 'Color',[1 0 0]);
            else
                plot(StopT, 'Color',[0.5 0 0]);
            end
        elseif  ~isempty(strfind(currentFile, 'stimon'))
            if ~isempty(strfind(currentFile, 'left'))
                plot(StopT, 'Color',[0 1 0]);
            else
                plot(StopT, 'Color',[0 0.5 0]);
            end
        elseif  ~isempty(strfind(currentFile, 'stimoff'))
            if ~isempty(strfind(currentFile, 'left'))
                plot(StopT, 'Color',[0 0 1]);
            else
                plot(StopT, 'Color',[0 0 0.5]);
            end
        end
    end
    keyboard
    title('Postop. stimon performance')
    saveas(gcf,fullfile('E:\drive_backup\Experiments\Human_SSRT\Analysis\Human\_behavior',[humanID '_SSDplot.fig']));
    close(gcf)
end
