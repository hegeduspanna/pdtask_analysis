function Human_SSRT_analysis_main(behav, rec, iseeg)
%   HUMAN_SSRT_ANALYSIS_MAIN(BEHAV,REC,EEG) main function for human SSRT task
%   data analysis.
%   HUMAN_SSRT_ANALYSIS_MAIN is analyzing behavioral, neuronal and EEG data
%   recorded during an SSRT task in Parkinsonian patients. BEHAV, REC and
%   EEG logical inputs are controlling the need for behavioral, electrode or EEg
%   data analysis, respectively.

% 2020-Jun-11

narginchk(0,3) % check no. of inputs
if nargin < 1
    behav = 1
end

if nargin < 2
    rec = 1
end

if nargin < 3
    iseeg = 1
end

% issync = iseeg(1);
% intraop_eeg = iseeg(2);

resdir = fullfile(getpref('cellbase', 'datapath'),'ANALYSIS'); % results directory
if ~isfolder(resdir) % if not exist - make
    mkdir(resdir)
end


rootdir = getpref('cellbase', 'datapath'); % cellbase source directory

%%%%%%%%%%% PREOP BEHAVIORAL ANALYSIS %%%%%%%%%%%%
if behav 
    rootdir = 'D:\PD_cellbase';
    measured_patients = {'pd01' 'pd02' 'pd03' 'pd04' 'pd05' 'pd07' 'pd08' 'pd10' 'pd12' 'pd13'};
    
    [preop_RT, postop_stimon_RT, postop_stimoff_RT, FA_preop_RT, FA_postop_son_RT, FA_postop_soff_RT, preop_GoRT, postop_son_GoRT, postop_soff_GoRT]  = deal(cell(1,length(measured_patients)));
    [preop_p, postop_son_p, postop_soff_p] = deal(nan(1,length(measured_patients)));
    for i = 1:length(measured_patients)
        % Compare RT
        sessionfolder1 = choose_session(measured_patients{i}, 'preop')
        sessionfolder2 = choose_session(measured_patients{i}, 'postop')
        
        preopdir = fullfile(rootdir,measured_patients{i}, sessionfolder1);
        preop_files = dir(preopdir);
        for j = 1:length(preop_files)
            if ~isempty(strfind(preop_files(j).name, 'pdtask'))
                PreopResults = load(fullfile(preopdir, preop_files(j).name));
                preop_RT{i} = PreopResults.Results.Trial.RT1_ARD-PreopResults.Results.Trial.CUE_ARD; % preop. reaction time
                
                NumFlip = length(PreopResults.Results.Trial.flipTime);   % Number of Cue presentations
                Hit = PreopResults.Results.Trial.outcomeCode(1:NumFlip) == 1; % Number of hits
                NoStopTrials = PreopResults.Results.Trial.outcomeCode==1 | PreopResults.Results.Trial.outcomeCode ==2; % number of non-stop trials
                preop_p(i) = sum(Hit)/sum(NoStopTrials)
                
                StopTrials = PreopResults.Results.Trial.outcomeCode==5;
                FA_preop_RT{i} = preop_RT{i}(StopTrials);
                
                NoStopTrials = PreopResults.Results.Trial.outcomeCode==1 | PreopResults.Results.Trial.outcomeCode ==2 |  PreopResults.Results.Trial.outcomeCode ==3 |  PreopResults.Results.Trial.outcomeCode ==4 ;
                preop_GoRT{i} = preop_RT{i}(NoStopTrials);
                preop_RT{i} = preop_RT{i}(2:end-1); % remove first and last trial                
            end
        end
        
        postopdir = fullfile(rootdir,measured_patients{i}, sessionfolder2);
        postop_files = dir(fullfile(rootdir,measured_patients{i}, sessionfolder2));
        for j = 1:length(postop_files)
            if ~isempty(strfind(postop_files(j).name, 'pdtask'))&&~isempty(strfind(postop_files(j).name, 'stimon'))
                PostopResults_stimon = load(fullfile(postopdir, postop_files(j).name));
                postop_stimon_RT{i} = PostopResults_stimon.Results.Trial.RT1_ARD-PostopResults_stimon.Results.Trial.CUE_ARD;
                
                NumFlip2 = length(PostopResults_stimon.Results.Trial.flipTime);   % Number of Cue presentations
                Hit2 = PostopResults_stimon.Results.Trial.outcomeCode(1:NumFlip2) == 1; % Number of hits
                NoStopTrials2 = PostopResults_stimon.Results.Trial.outcomeCode==1 | PostopResults_stimon.Results.Trial.outcomeCode ==2; % number of non-stop trials
                postop_son_p(i) = sum(Hit2)/sum(NoStopTrials2)
                
                StopTrials = PostopResults_stimon.Results.Trial.outcomeCode==5;
                FA_postop_son_RT{i} = postop_stimon_RT{i}(StopTrials);
                
                NoStopTrials = PostopResults_stimon.Results.Trial.outcomeCode==1 | PostopResults_stimon.Results.Trial.outcomeCode ==2 |  PostopResults_stimon.Results.Trial.outcomeCode ==3 |  PostopResults_stimon.Results.Trial.outcomeCode ==4 ;
                postop_son_GoRT{i} = postop_stimon_RT{i}(NoStopTrials);
                postop_stimon_RT{i} = postop_stimon_RT{i}(2:end-1);
            end
            
            if ~isempty(strfind(postop_files(j).name, 'pdtask'))&&~isempty(strfind(postop_files(j).name, 'stimoff'))
                PostopResults_stimoff = load(fullfile(postopdir, postop_files(j).name));
                postop_stimoff_RT{i} = PostopResults_stimoff.Results.Trial.RT1_ARD-PostopResults_stimoff.Results.Trial.CUE_ARD;
                
                NumFlip3 = length(PostopResults_stimoff.Results.Trial.flipTime);   % Number of Cue presentations
                Hit3 = PostopResults_stimoff.Results.Trial.outcomeCode(1:NumFlip3) == 1; % Number of hits
                NoStopTrials3 = PostopResults_stimoff.Results.Trial.outcomeCode==1 | PostopResults_stimoff.Results.Trial.outcomeCode ==2; % number of non-stop trials
                postop_soff_p(i) = sum(Hit3)/sum(NoStopTrials3)
                
                StopTrials = PostopResults_stimoff.Results.Trial.outcomeCode==5;
                FA_postop_soff_RT{i} = postop_stimoff_RT{i}(StopTrials);
                
                NoStopTrials = PostopResults_stimoff.Results.Trial.outcomeCode==1 | PostopResults_stimoff.Results.Trial.outcomeCode ==2 |  PostopResults_stimoff.Results.Trial.outcomeCode ==3 |  PostopResults_stimoff.Results.Trial.outcomeCode ==4 ;
                postop_soff_GoRT{i} = postop_stimoff_RT{i}(NoStopTrials);
                postop_stimoff_RT{i} = postop_stimoff_RT{i}(2:end-1);
            end
        end
        
        %Make boxplots
        x = [preop_RT{i} postop_stimon_RT{i} postop_stimoff_RT{i}];
        g = [ones(size(preop_RT{i})) 2*ones(size(postop_stimon_RT{i})) 3*ones(size(postop_stimoff_RT{i}))];
        figure;
        boxplot(x,g)
        saveas(gcf, fullfile('D:\Plots\_BEHAVIOR\',[measured_patients{i} '_RT.fig']));
        close(gcf)
        
        boxstat(preop_RT{i}, postop_stimon_RT{i}, 'preop', 'postop stimon', 0.05)
        saveas(gcf, fullfile('D:\Plots\_BEHAVIOR\',[measured_patients{i} '_RT_stat.fig']));
        close(gcf)
        
        % Compare performance for individuals
        figure; plot([1 2 3],[preop_p(i),postop_son_p(i),postop_soff_p(i)]);
        title(measured_patients{i})
        
        % Compare false alarm reaction time
%                  x = [FA_preop_RT{i} FA_postop_son_RT{i} FA_postop_soff_RT{i}];
%                 g = [ones(size(FA_preop_RT{i})) 2*ones(size(FA_postop_son_RT{i})) 3*ones(size(FA_postop_soff_RT{i}))];
%                 figure;
%                 boxplot(x,g)
%                 saveas(gcf, fullfile('D:\Plots\_BEHAVIOR\',[measured_patients{i} '_RT_FA.fig']));
%                 close(gcf)
%         
%                 boxstat(FA_preop_RT{i}, FA_postop_son_RT{i}, 'preop', 'postop stimon', 0.05)
%                 saveas(gcf, fullfile('D:\Plots\_BEHAVIOR\',[measured_patients{i} '_RT_stat_FA.fig']));
%                 close(gcf)
    end
    
    % Average Reaction time of patients
    inx = [1 3 4 6 8 9 10];
    inx2 = [2 5 7];
    
%     preop_median = nanmedian(horzcat(preop_RT{inx}));
%     postop_son_median = nanmedian(horzcat(postop_stimon_RT{inx}));
%     postop_soff_median = nanmedian(horzcat(postop_stimoff_RT{inx}));
%     
%     preop_median2 = nanmedian(horzcat(preop_RT{inx2}));
%     postop_son_median2 = nanmedian(horzcat(postop_stimon_RT{inx2}));
%     postop_soff_median2 = nanmedian(horzcat(postop_stimoff_RT{inx2}));
%     
%     se1_rt = se_of_median(horzcat(preop_RT{inx}));
%     se2_rt = se_of_median(horzcat(postop_stimon_RT{inx}));
%     se3_rt = se_of_median(horzcat(postop_stimoff_RT{inx}));
%     
%     se4_rt = se_of_median(horzcat(preop_RT{inx2}));
%     se5_rt = se_of_median(horzcat(postop_stimon_RT{inx2}));
%     se6_rt = se_of_median(horzcat(postop_stimoff_RT{inx2}));
%     
%     err1 = [se1_rt se2_rt se3_rt];
%     err2 = [se4_rt se5_rt se6_rt];
%     figure;
%     errorbar([1 2 3], [preop_median, postop_son_median, postop_soff_median], err1)
%     hold on
%     errorbar([1 2 3], [preop_median2, postop_son_median2, postop_soff_median2], err2)
%     boxstat(horzcat(preop_RT{inx}), horzcat(postop_stimon_RT{inx}), 'preop', 'postop stimon', 0.05)
%     boxstat(horzcat(postop_stimon_RT{inx}),  horzcat(postop_stimoff_RT{inx}), 'preop', 'postop stimoff', 0.05)
    
    %Average false alarm reaction time
%      preop_median = nanmedian(horzcat(FA_preop_RT{inx}));
%     postop_son_median = nanmedian(horzcat(FA_postop_son_RT{inx}));
%     postop_soff_median = nanmedian(horzcat(FA_postop_soff_RT{inx}));
%     
%     preop_median2 = nanmedian(horzcat(FA_preop_RT{inx2}));
%     postop_son_median2 = nanmedian(horzcat(FA_postop_son_RT{inx2}));
%     postop_soff_median2 = nanmedian(horzcat(FA_postop_soff_RT{inx2}));
%     
%     se1_rt = se_of_median(horzcat(FA_preop_RT{inx}));
%     se2_rt = se_of_median(horzcat(FA_postop_son_RT{inx}));
%     se3_rt = se_of_median(horzcat(FA_postop_soff_RT{inx}));
%     
%     se4_rt = se_of_median(horzcat(FA_preop_RT{inx2}));
%     se5_rt = se_of_median(horzcat(FA_postop_son_RT{inx2}));
%     se6_rt = se_of_median(horzcat(FA_postop_soff_RT{inx2}));
%     
%     err1 = [se1_rt se2_rt se3_rt];
%     err2 = [se4_rt se5_rt se6_rt];
%     figure;
%     errorbar([1 2 3], [preop_median, postop_son_median, postop_soff_median], err1)
%     hold on
%     errorbar([1 2 3], [preop_median2, postop_son_median2, postop_soff_median2], err2)
%     boxstat(horzcat(FA_preop_RT{inx}), horzcat(FA_postop_son_RT{inx}), 'preop', 'postop stimon', 0.05)
%     boxstat(horzcat(FA_postop_son_RT{inx}),  horzcat(FA_postop_soff_RT{inx}), 'preop', 'postop stimoff', 0.05)
    
    % Average Go RT 
%     preop_median = nanmedian(horzcat(preop_GoRT{inx}));
%     postop_son_median = nanmedian(horzcat(postop_son_GoRT{inx}));
%     postop_soff_median = nanmedian(horzcat(postop_soff_GoRT{inx}));
%     
%     preop_median2 = nanmedian(horzcat(preop_GoRT{inx2}));
%     postop_son_median2 = nanmedian(horzcat(postop_son_GoRT{inx2}));
%     postop_soff_median2 = nanmedian(horzcat(postop_soff_GoRT{inx2}));
%     
%     se1_rt = se_of_median(horzcat(preop_GoRT{inx}));
%     se2_rt = se_of_median(horzcat(postop_son_GoRT{inx}));
%     se3_rt = se_of_median(horzcat(postop_soff_GoRT{inx}));
%     
%     se4_rt = se_of_median(horzcat(preop_GoRT{inx2}));
%     se5_rt = se_of_median(horzcat(postop_son_GoRT{inx2}));
%     se6_rt = se_of_median(horzcat(postop_soff_GoRT{inx2}));
%     
%     err1 = [se1_rt se2_rt se3_rt];
%     err2 = [se4_rt se5_rt se6_rt];
%     figure;
%     errorbar([1 2 3], [preop_median, postop_son_median, postop_soff_median], err1)
%     hold on
%     errorbar([1 2 3], [preop_median2, postop_son_median2, postop_soff_median2], err2)
%     boxstat(horzcat(preop_GoRT{inx}), horzcat(postop_son_GoRT{inx}), 'preop', 'postop stimon', 0.05)
%     boxstat(horzcat(postop_son_GoRT{inx}),  horzcat(postop_soff_GoRT{inx}), 'preop', 'postop stimoff', 0.05)
    
    
    % Average performance for patients with increased RT
    se1 = se_of_median(preop_p(inx))
    se2 = se_of_median(postop_son_p(inx))
    se3 = se_of_median(postop_soff_p(inx))
    err = [se1 se2 se3]
    
    
    se4 = se_of_median(preop_p(inx2))
    se5 = se_of_median(postop_son_p(inx2))
    se6 = se_of_median(postop_soff_p(inx2))
    err2 = [se4 se5 se6];
    figure;
    errorbar([1 2 3], [median(preop_p(inx)),median(postop_son_p(inx)), median(postop_soff_p(inx))], err)
    hold on
        errorbar([1 2 3], [median(preop_p(inx2)),median(postop_son_p(inx2)), median(postop_soff_p(inx2))], err2)

    
    % Statistics
    boxstat(preop_p(inx), postop_son_p(inx), 'preop', 'postop stimon', 0.05)
    boxstat( postop_son_p(inx),  postop_soff_p(inx), 'preop', 'postop stimoff', 0.05)

end

if rec
end



    if issync
        triggersync_eeg(patient, session, 'stimon');
    end
    
    if intraop_eeg
    patients = {'pd02' 'pd03' 'pd04' 'pd05' 'pd07' 'pd08' 'pd10' 'pd12' 'pd13'};
    intraop_sessions = { '181123l' '181130l' '190118l' '190201l' '190405l' '190412l' '191009l' '191025l' '200121l'};
    
    for p =1:length(patients)
        cp = patients{p};
        cs = intraop_sessions{p};
        plotdir = 'G:\Plots\_INTRAOP\_ETA';
        PDTask_sta(cp, cs, resdir, plotdir)
    end
    end
    
    
    
    all_sta=cell(64,7);
    cond = {'stimoff' 'stimon'};
    for j = 1 :length(cond)
        stim_cond = cond{j};
        for i=1:length(patients)
            cpatient = patients{i};
            csession = sessions{i};
            [filtered_data, labels]= EEG_analysis_master(cpatient, csession, resdir, stim_cond, false);
            for k=1:length(labels)
                [all_sta{k,i}]=postop_sta(cpatient, csession, filtered_data(k,:), labels{k}, stim_cond);
            end
        end
        keyboard
    end


function sessionfolder = choose_session(patient, tag)
switch tag
    case 'preop'
        if strcmp(patient, 'pd01')
            sessionfolder = '181115l';
        elseif strcmp(patient, 'pd02')
            sessionfolder = '181122l';
        elseif strcmp(patient, 'pd03')
            sessionfolder = '181129l';
        elseif strcmp(patient, 'pd04')
            sessionfolder = '190117l';
        elseif strcmp(patient, 'pd05')
            sessionfolder = '190131l';
        elseif strcmp(patient, 'pd07')
            sessionfolder = '190404l';
        elseif strcmp(patient, 'pd08')
            sessionfolder = '190411l';
        elseif strcmp(patient, 'pd10')
            sessionfolder = '191008l';
        elseif strcmp(patient, 'pd12')
            sessionfolder = '191024l';
        elseif strcmp(patient, 'pd13')
            sessionfolder = '200107l';
        end
    case 'postop'
        if strcmp(patient, 'pd01')
            sessionfolder = '190516l';
        elseif strcmp(patient, 'pd02')
            sessionfolder = '200127l';
        elseif strcmp(patient, 'pd03')
            sessionfolder = '190613l';
        elseif strcmp(patient, 'pd04')
            sessionfolder = '191021l';
        elseif strcmp(patient, 'pd05')
            sessionfolder = '191111l';
        elseif strcmp(patient, 'pd07')
            sessionfolder = '191014l';
        elseif strcmp(patient, 'pd08')
            sessionfolder = '190709l';
        elseif strcmp(patient, 'pd10')
            sessionfolder = '200720l';
        elseif strcmp(patient, 'pd12')
            sessionfolder = '200702l';
        elseif strcmp(patient, 'pd13')
            sessionfolder = '200716l';
        end
end
